<?php

namespace App\Repository;

use App\Entity\Voeux;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Voeux|null find($id, $lockMode = null, $lockVersion = null)
 * @method Voeux|null findOneBy(array $criteria, array $orderBy = null)
 * @method Voeux[]    findAll()
 * @method Voeux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoeuxRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Voeux::class);
    }

//    /**
//     * @return Voeux[] Returns an array of Voeux objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Voeux
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
