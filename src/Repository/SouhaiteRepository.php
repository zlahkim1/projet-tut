<?php

namespace App\Repository;

use App\Entity\Souhaite;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Souhaite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Souhaite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Souhaite[]    findAll()
 * @method Souhaite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SouhaiteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Souhaite::class);
    }

//    /**
//     * @return Souhaite[] Returns an array of Souhaite objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Souhaite
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
