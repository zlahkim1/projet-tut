<?php

namespace App\Repository;

use App\Entity\Correspond;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Correspond|null find($id, $lockMode = null, $lockVersion = null)
 * @method Correspond|null findOneBy(array $criteria, array $orderBy = null)
 * @method Correspond[]    findAll()
 * @method Correspond[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorrespondRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Correspond::class);
    }

//    /**
//     * @return Correspond[] Returns an array of Correspond objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Correspond
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
