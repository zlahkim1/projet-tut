<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 22/11/2018
 * Time: 10:03
 */

namespace App\Controller;

use App\Entity\Departement;
use App\Entity\Etablissement;
use App\Entity\TypeFormation;
use App\Entity\User;
use App\Entity\Ville;
use App\Entity\Voeux;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;



use Twig\Environment;                            // template TWIG
use Symfony\Bridge\Doctrine\RegistryInterface;   // ORM Doctrine
use Symfony\Component\HttpFoundation\Request;    // objet REQUEST
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
// dans les annotations @Method

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;  // annotation security


class VoeuxController extends Controller
{

/**
 * @Route("/voeux/show", name = "voeux.show")
 */
    public function showVoeux(Request $request, Environment $twig, RegistryInterface $doctrine)
    {
        $voeux = $doctrine->getRepository(Voeux::class)->findAll();
        return new Response($twig->render('frontOff/frontOFFICE.html.twig', ['voeux' => $voeux ]));

    }
 /**
  * @Route("Voeux/add", name = "voeux.add")
  */
    public function addVoeux(Request $request, Environment $twig, RegistryInterface $doctrine){
        $type_voeux = $doctrine->getRepository(TypeFormation::class)->findAll();
        return new Response($twig->render('frontOff/addVoeux.html.twig',['typeVoeux'=>$type_voeux]));
    }


    /**
     * @Route("/voeux/validAdd", name="Voeux.validFormAddVoeux", methods={"POST"})
     */
    public function validFormAddVoeux(Request $request, Environment $twig, RegistryInterface $doctrine)
    {


        $donnees['ville'] = htmlspecialchars($_POST['ville']);
        $donnees['departement'] = htmlspecialchars($_POST['departement']);
        $donnees['etablissement'] = htmlspecialchars($_POST['etablissement']);
        $donnees['url'] = $request->request->get('url');
        $donnees['formation_id'] = htmlentities($request->request->get('formation_id'));
        $donnees['date_limite'] = htmlentities($_POST['date_limite']);
        $erreurs = array();
        if ((!preg_match("/^[A-Za-z ]{2,}/", $donnees['ville']))) $erreurs['ville'] = 'nom composé de 2 lettres minimum';
        if ((!preg_match("/^[A-Za-z ]{2,}/", $donnees['departement']))) $erreurs['departement'] = 'nom composé de 2 lettres minimum';
        if ((!preg_match("/^[A-Za-z ]{2,}/", $donnees['etablissement']))) $erreurs['etablissement'] = 'nom composé de 2 lettres minimum';
        if ((!preg_match("/^[A-Za-z ]{2,}/", $donnees['url']))) $erreurs['url'] = 'nom composé de 2 lettres minimum';
        if (!is_numeric($donnees['formation_id'])) $erreurs['formation_id'] = 'veuillez saisir une valeur';


        if (!empty($erreurs)) {
            // A modifier
            $repository = $doctrine->getRepository(TypeFormation::class);
            $typeFormation = $repository->findBy(
                array(),
                array('id' => 'ASC', 'libelle' => 'ASC'),
                3,
                0

            );
            return $this->render('produit/addProduit.html.twig', ['donnees' => $donnees, 'erreurs' => $erreurs, 'typeFormation' => $typeFormation]);

        }else{
            $em = $this->container->get('doctrine')->getManager();
            $type = $doctrine->getRepository(TypeFormation::class)->find('formation_id');
            $new_voeux = new Voeux();

            $new_dep = new Departement();
            $new_eta = new Etablissement();
            $new_ville = new Ville();

            $new_dep->setNomDep($donnees['departement']);
            $new_eta->setNomEta($donnees['etablissement']);
            $new_ville->setNomVille($donnees['ville']);

            $dep = $doctrine->getRepository(Departement::class)->findOneBy(['nom_dep'=> $donnees['departement']]);





            $new_voeux->setUrl($donnees['url']);
            $new_voeux->setDateLimite(new \DateTime($donnees['date_limite']));
            $new_voeux->setDepartementId($donnees['prix']);
            $new_voeux->setTypeProduitId($type);
            dump($new_produits->getTypeProduitId());


            $em->persist($new_produits);  // met l'opération en mémoire (planification de l'opération)

            $em->flush();    // commit des opérations


            return $this->redirectToRoute('Produit.showProduits');
        }
    }
}