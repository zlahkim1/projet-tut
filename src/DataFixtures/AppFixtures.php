<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\CommentaireProduit;
use App\Entity\Etat;
use App\Entity\Produit;
use App\Entity\TypeProduit;
use App\Entity\User;
use App\Entity\Panier;
use App\Entity\Commande;
use App\Entity\LigneCommande;

class AppFixtures extends Fixture
{
    // ...https://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }


    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);

        ////////////
        /// TD
        ///
//        $this->testAjoutPanier($manager);
//        $this->affichePanier($manager);
//        $this->modifQuantite($manager);
//        $this->affichePanier($manager);
//        $this->transformePanierCommande($manager);
//        $this->affichePanier($manager);
//        $this->testAjoutPanier($manager);
    }


    public function loadUsers(objectManager $manager){
        // les utilisateurs

        echo " \n\nles utilisateurs : \n";

        $admin = new User();
        $password = $this->encoder->encodePassword($admin, 'admin');
        $admin->setPassword($password);
        $admin->setRoles('ROLE_ADMIN')
            ->setUsername('admin')->setEmail('admin@example.com')->setIsActive('1');
        $manager->persist($admin);
        echo $admin."\n";

        $client = new User();
        $client->setRoles('ROLE_CLIENT')
            ->setUsername('client')->setEmail('client@example.com')->setIsActive('1');
        $password = $this->encoder->encodePassword($client, 'client');
        $client->setPassword($password);
        $manager->persist($client);
        echo $client."\n";

        $client2 = new User();
        $client2->setRoles('ROLE_CLIENT')
            ->setUsername('client2')->setEmail('client2@example.com')->setIsActive('1');
        $password = $this->encoder->encodePassword($client2, 'client2');
        $client2->setPassword($password);
        $manager->persist($client2);
        echo $client2."\n";
        $manager->flush();

    }


}