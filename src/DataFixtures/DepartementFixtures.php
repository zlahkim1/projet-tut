<?php

namespace App\DataFixtures;

use App\Entity\Departement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DepartementFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->addDepartement($manager);
    }

    public function addDepartement(ObjectManager $manager)
    {
        $deps=[['id'=>1,'nom_dep' =>'Ain'],
            ['id'=>2,   'nom_dep' =>'Aisne'],
            ['id'=>3,   'nom_dep' =>'Allier'],
            ['id'=>4,   'nom_dep' =>'Alpes-de-Haute-Provence'],
            ['id'=>5,   'nom_dep' =>'Hautes-alpes'],
            ['id'=>6,   'nom_dep' =>'Alpes-maritimes'],
            ['id'=>7,   'nom_dep' =>'Ardèche'],
            ['id'=>8,   'nom_dep' =>'Ardennes'],
            ['id'=>9,   'nom_dep' =>'Ariège'],
            ['id'=>10,  'nom_dep' =>'Aube'],
            ['id'=>11,  'nom_dep' => 'Aude'],
            ['id'=>12,  'nom_dep' =>'Aveyron'],
            ['id'=>13,  'nom_dep' =>'Bouches-du-Rhône'],
            ['id'=>14,  'nom_dep' =>'Calvados'],
            ['id'=>15,  'nom_dep' =>'Cantal'],
            ['id'=>16,  'nom_dep' =>'Charente'],
            ['id'=>17,  'nom_dep' =>'Charente-maritime'],
            ['id'=>18,  'nom_dep' =>'Cher'],
            ['id'=>19,  'nom_dep' =>'Corrèze'],
            ['id'=>400, 'nom_dep' =>'Corse-du-sud '],
            ['id'=>400, 'nom_dep' =>'Haute-Corse'],
            ['id'=>21,  'nom_dep' =>'Cote-d Or'],
            ['id'=>22,  'nom_dep' =>'Côtes-d Armor'],
            ['id'=>23,  'nom_dep' =>'Creuse'],
            ['id'=>24,  'nom_dep' =>'Dordogne'],
            ['id'=>25,  'nom_dep' =>'Doubs'],
            ['id'=>26,  'nom_dep' =>'Drôme'],
            ['id'=>27,  'nom_dep' =>'Eure'],
            ['id'=>28,  'nom_dep' =>'Eure-et-loir'],
            ['id'=>29,  'nom_dep' =>'Finistère'],
            ['id'=>30,  'nom_dep' =>'Gard'],
            ['id'=>31,  'nom_dep' =>'Haute-garonne'],
            ['id'=>32,  'nom_dep' =>'Gers'],
            ['id'=>33,  'nom_dep' =>'Gironde'],
            ['id'=>34,  'nom_dep' =>'Hérault'],
            ['id'=>35,  'nom_dep' =>'Ille-et-vilaine'],
            ['id'=>36,  'nom_dep' =>'Indre'],
            ['id'=>37,  'nom_dep' =>'Indre-et-loire'],
            ['id'=>38,  'nom_dep' =>'Isère'],
            ['id'=>39,  'nom_dep' =>'Jura'],
            ['id'=>40,  'nom_dep' =>'Landes'],
            ['id'=>41,  'nom_dep' =>'Loir-et-cher'],
            ['id'=>42,  'nom_dep' =>'Loire'],
            ['id'=>43,  'nom_dep' =>'Haute-loire'],
            ['id'=>44,  'nom_dep' =>'Loire-atlantique'],
            ['id'=>45,  'nom_dep' =>'Loiret'],
            ['id'=>46,  'nom_dep' =>'Lot'],
            ['id'=>47,  'nom_dep' =>'Lot-et-garonne'],
            ['id'=>48,  'nom_dep' =>'Lozère'],
            ['id'=>49,  'nom_dep' =>'Maine-et-loire'],
            ['id'=>50,  'nom_dep' =>'Manche'],
            ['id'=>51,  'nom_dep' =>'Marne'],
            ['id'=>52,  'nom_dep' =>'Haute-marne'],
            ['id'=>53,  'nom_dep' =>'Mayenne'],
            ['id'=>54,  'nom_dep' =>'Meurthe-et-moselle'],
            ['id'=>55,  'nom_dep' =>'Meuse'],
            ['id'=>56,  'nom_dep' =>'Morbihan'],
            ['id'=>57,  'nom_dep' =>'Moselle'],
            ['id'=>58,  'nom_dep' =>'Nièvre'],
            ['id'=>59,  'nom_dep' =>'Nord'],
            ['id'=>60,  'nom_dep' =>'Oise'],
            ['id'=>61,  'nom_dep' =>'Orne'],
            ['id'=>62,  'nom_dep' =>'Pas-de-calais'],
            ['id'=>63,  'nom_dep' =>'Puy-de-dôme'],
            ['id'=>64,  'nom_dep' =>'Pyrénées-atlantiques'],
            ['id'=>65,  'nom_dep' =>'Hautes-Pyrénées'],
            ['id'=>66,  'nom_dep' =>'Pyrénées-orientales'],
            ['id'=>67,  'nom_dep' =>'Bas-rhin'],
            ['id'=>68,  'nom_dep' =>'Haut-rhin'],
            ['id'=>69,  'nom_dep' =>'Rhône'],
            ['id'=>70,  'nom_dep' =>'Haute-saône'],
            ['id'=>71,  'nom_dep' =>'Saône-et-loire'],
            ['id'=>72,  'nom_dep' =>'Sarthe'],
            ['id'=>73,  'nom_dep' =>'Savoie'],
            ['id'=>74,  'nom_dep' =>'Haute-savoie'],
            ['id'=>75,  'nom_dep' =>'Paris'],
            ['id'=>76,  'nom_dep' =>'Seine-maritime'],
            ['id'=>77,  'nom_dep' =>'Seine-et-marne'],
            ['id'=>78,  'nom_dep' =>'Yvelines'],
            ['id'=>79,  'nom_dep' =>'Deux-sèvres'],
            ['id'=>80,  'nom_dep' =>'Somme'],
            ['id'=>81,  'nom_dep' =>'Tarn'],
            ['id'=>82,  'nom_dep' =>'Tarn-et-garonne'],
            ['id'=>83,  'nom_dep' =>'Var'],
            ['id'=>84,  'nom_dep' =>'Vaucluse'],
            ['id'=>85,  'nom_dep' =>'Vendée'],
            ['id'=>86,  'nom_dep' =>'Vienne'],
            ['id'=>87,  'nom_dep' =>'Haute-vienne'],
            ['id'=>88,  'nom_dep' =>'Vosgesnal'],
            ['id'=>89,  'nom_dep' =>'Yonne'],
            ['id'=>90,  'nom_dep' =>'Territoire de belfort'],
            ['id'=>91,  'nom_dep' =>'Essonne'],
            ['id'=>92,  'nom_dep' =>'Hauts-de-seine'],
            ['id'=>93,  'nom_dep' =>'Seine-Saint-Denis'],
            ['id'=>94,  'nom_dep' =>'Val-de-marne'],
            ['id'=>95,  'nom_dep' =>'Val-doise'],
            ['id'=>971, 'nom_dep' =>'Guadeloupe'],
            ['id'=>972, 'nom_dep' =>'Martinique'],
            ['id'=>973, 'nom_dep' =>'Guyane'],
            ['id'=>974, 'nom_dep' =>'La réunion'],
            ['id'=>976, 'nom_dep' =>'Mayotte']

            ];

        foreach ($deps as $dep) {
            $new_dep = new Departement();
            $new_dep->setNomDep($dep['nom_dep']);
            //$manager->persist($new_dep);
            //$manager->flush();
        }
    }


}
