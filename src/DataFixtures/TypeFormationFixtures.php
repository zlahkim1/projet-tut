<?php

namespace App\DataFixtures;

use App\Entity\TypeFormation;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\ExpressionLanguage\Tests\Node\Obj;

class TypeFormationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->addTypeFormation($manager);
    }

    public function addTypeFormation(ObjectManager $manager){
        $type_forms = [['id'=>1, 'nom_formation' =>'EI'],
            ['id'=>2, 'nom_formation' =>'LI'],
            ['id'=>3, 'nom_formation' =>'LPRO'],
            ['id'=>4, 'nom_formation' =>'EI ALT'],
            ['id'=>5, 'nom_formation' =>'MIAGE'],
            ['id'=>6, 'nom_formation' =>'AUTRE'],
        ];
        foreach ($type_forms as $type_form) {
            $new_type_for = new TypeFormation();
            $new_type_for->setNomFormation($type_form['nom_formation']);
            $manager->persist($new_type_for);
            $manager->flush();
        }
    }
}
