<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoeuxRepository")
 */
class Voeux
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $date_limite;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_voeux;

    /**
     * @ORM\ManyToOne(targetEntity="Ville")
     * @ORM\JoinColumn(name="ville_voeux_id", referencedColumnName="id" ,onDelete="CASCADE")
     */
    private $ville_voeux_id;

    /**
     * @ORM\ManyToOne(targetEntity="Departement")
     * @ORM\JoinColumn(name="departement_id", referencedColumnName="id" ,onDelete="CASCADE")
     */
    private $departement_id;

    /**
     * @ORM\ManyToOne(targetEntity="TypeFormation")
     * @ORM\JoinColumn(name="formation_id", referencedColumnName="id" ,onDelete="CASCADE")
     */
    private $formation_id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id" ,onDelete="CASCADE")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="Etablissement")
     * @ORM\JoinColumn(name="etablissement_id", referencedColumnName="id" ,onDelete="CASCADE")
     */
    private $etablissement_id;


    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateLimite(): ?\DateTimeInterface
    {
        return $this->date_limite;
    }

    public function setDateLimite(\DateTimeInterface $date_limite): self
    {
        $this->date_limite = $date_limite;

        return $this;
    }

    public function getNomVoeux(): ?string
    {
        return $this->nom_voeux;
    }

    public function setNomVoeux(string $nom_voeux): self
    {
        $this->nom_voeux = $nom_voeux;

        return $this;
    }

    public function getVilleVoeuxId(): ?int
    {
        return $this->ville_voeux_id;
    }

    public function setVilleVoeuxId(int $ville_voeux_id): self
    {
        $this->ville_voeux_id = $ville_voeux_id;

        return $this;
    }

    public function getDepartementId(): ?int
    {
        return $this->departement_id;
    }

    public function setDepartementId(int $departement_id): self
    {
        $this->departement_id = $departement_id;

        return $this;
    }

    public function getFormationId(): ?int
    {
        return $this->formation_id;
    }

    public function setFormationId(int $formation_id): self
    {
        $this->formation_id = $formation_id;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getEtablissementId(): ?int
    {
        return $this->etablissement_id;
    }

    public function setEtablissementId(int $etablissement_id): self
    {
        $this->etablissement_id = $etablissement_id;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}
