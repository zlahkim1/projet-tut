<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CorrespondRepository")
 */
class Correspond
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_Voeux;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdVoeux(): ?int
    {
        return $this->id_Voeux;
    }

    public function setIdVoeux(int $id_Voeux): self
    {
        $this->id_Voeux = $id_Voeux;

        return $this;
    }
}
