<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtablissementRepository")
 */
class Etablissement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom_eta;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomEta(): ?string
    {
        return $this->nom_eta;
    }

    public function setNomEta(string $nom_eta): self
    {
        $this->nom_eta = $nom_eta;

        return $this;
    }


    public function getVilleId(): ?int
    {
        return $this->ville_id;
    }

    public function setVilleId(int $ville_id): self
    {
        $this->ville_id = $ville_id;

        return $this;
    }
}
