<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormuleRepository")
 */
class Formule
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $avis_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $voeux_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $remarque;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAvisId(): ?int
    {
        return $this->avis_id;
    }

    public function setAvisId(int $avis_id): self
    {
        $this->avis_id = $avis_id;

        return $this;
    }

    public function getVoeuxId(): ?int
    {
        return $this->voeux_id;
    }

    public function setVoeuxId(int $voeux_id): self
    {
        $this->voeux_id = $voeux_id;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(?string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }
}
