<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181107221103 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE avis (id INT AUTO_INCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE correspond (id INT AUTO_INCREMENT NOT NULL, id_voeux INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departement (id INT AUTO_INCREMENT NOT NULL, nom_dep VARCHAR(255) NOT NULL, ville_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE etablissement (id INT AUTO_INCREMENT NOT NULL, nom_eta VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formule (id INT AUTO_INCREMENT NOT NULL, avis_id INT NOT NULL, voeux_id INT NOT NULL, remarque VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE souhaite (id INT AUTO_INCREMENT NOT NULL, etudiant_id INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_formation (id INT AUTO_INCREMENT NOT NULL, nom_formation VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE app_users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(64) NOT NULL, email VARCHAR(60) NOT NULL, is_active TINYINT(1) NOT NULL, avis_id_prof VARCHAR(255) NOT NULL, roles VARCHAR(64) NOT NULL, UNIQUE INDEX UNIQ_C2502824F85E0677 (username), UNIQUE INDEX UNIQ_C2502824E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ville (id INT AUTO_INCREMENT NOT NULL, nom_ville VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE voeux (id INT AUTO_INCREMENT NOT NULL, ville_voeux_id INT DEFAULT NULL, departement_id INT DEFAULT NULL, formation_id INT DEFAULT NULL, user_id INT DEFAULT NULL, etablissement_id INT DEFAULT NULL, date_limite DATE NOT NULL, nom_voeux VARCHAR(255) NOT NULL, INDEX IDX_917F7851EE82F47B (ville_voeux_id), INDEX IDX_917F7851CCF9E01E (departement_id), INDEX IDX_917F78515200282E (formation_id), INDEX IDX_917F7851A76ED395 (user_id), INDEX IDX_917F7851FF631228 (etablissement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE voeux ADD CONSTRAINT FK_917F7851EE82F47B FOREIGN KEY (ville_voeux_id) REFERENCES ville (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voeux ADD CONSTRAINT FK_917F7851CCF9E01E FOREIGN KEY (departement_id) REFERENCES departement (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voeux ADD CONSTRAINT FK_917F78515200282E FOREIGN KEY (formation_id) REFERENCES type_formation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voeux ADD CONSTRAINT FK_917F7851A76ED395 FOREIGN KEY (user_id) REFERENCES app_users (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE voeux ADD CONSTRAINT FK_917F7851FF631228 FOREIGN KEY (etablissement_id) REFERENCES etablissement (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE voeux DROP FOREIGN KEY FK_917F7851CCF9E01E');
        $this->addSql('ALTER TABLE voeux DROP FOREIGN KEY FK_917F7851FF631228');
        $this->addSql('ALTER TABLE voeux DROP FOREIGN KEY FK_917F78515200282E');
        $this->addSql('ALTER TABLE voeux DROP FOREIGN KEY FK_917F7851A76ED395');
        $this->addSql('ALTER TABLE voeux DROP FOREIGN KEY FK_917F7851EE82F47B');
        $this->addSql('DROP TABLE avis');
        $this->addSql('DROP TABLE correspond');
        $this->addSql('DROP TABLE departement');
        $this->addSql('DROP TABLE etablissement');
        $this->addSql('DROP TABLE formule');
        $this->addSql('DROP TABLE souhaite');
        $this->addSql('DROP TABLE type_formation');
        $this->addSql('DROP TABLE app_users');
        $this->addSql('DROP TABLE ville');
        $this->addSql('DROP TABLE voeux');
    }
}
